import mongoose from 'mongoose';
import courseModel from './models/course';

const resolvers = {
    // Query replaces Read (state invariant).
    Query: {
        // root is passed because of function definition, but we don't use it.
        allCourses: (root, {searchTerm}) => {
            if (searchTerm !== '') {
                return courseModel
                .find({ $text: { $search: searchTerm }})
                .sort({ voteCount: 'desc' });
            } else {
                return courseModel
                .find()
                .sort({ voteCount: 'desc' });
            }
        },
        course: (root, {id}) => {
            return courseModel.findOne({id: id});
        }
    },
    /** Mutations replaces Create, Update, Delete.
     * While query fields are executed in parallel, mutation fields run in series, one after the other.
     * https://graphql.org/learn/queries/
     */
    Mutation: {
        /** Structure: mutationType: resolverFunction
         * returnNewDocument: true returns the latest version of the document.
         */
        upvote: (root, {id}) => {
            return courseModel.findOneAndUpdate({id: id}, { $inc: { "voteCount" : 1 }}, { returnNewDocument: true });
        },
        downvote: (root, {id}) => {
            return courseModel.findOneAndUpdate({id: id}, { $inc: { "voteCount" : -1 }}, { returnNewDocument: true });
        },
        addCourse: (root, {title, author, description, topic, url}) => {
            // We don't assign an identifier since one is assigned automatically through uuid.
            const course = new courseModel({ 
                title: title, 
                author: author, 
                description: description, 
                topic: topic,
                url: url
            });
            return course.save();
        }
    }
}

export default resolvers;