import { Injectable } from '@angular/core';
import { Query, Course } from './types';
import { Apollo } from 'apollo-angular'; // Apollo Client for Angular.
import gql from 'graphql-tag'; // Used to write GraphQL query code.
import { map, filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  constructor(private apollo: Apollo) { }

  // Use Apollo Client to execute a query on the server.
  getAllCourses(searchTerm: String) {
    return this.apollo.watchQuery<Query>({
      pollInterval: 500,
      query: gql`
        query allCourses($searchTerm: String) {
          allCourses(searchTerm: $searchTerm) {
            id
            title
            author
            description
            topic
            url
            voteCount
          }
        }
      `,
      variables: {
        searchTerm: searchTerm
      }
    })
    .valueChanges
    .pipe(
      map(result => result.data.allCourses)
    );
  }

  upvoteCourse(id: string) {
    return this.apollo.mutate({
      mutation: gql`
        mutation upvote($id: String!) {
          upvote(id: $id) {
            id
            title
            voteCount
          }
        }
      `,
      variables: {
        id: id
      }
    });
  }

  downvoteCourse(id: string) {
    return this.apollo.mutate({
      mutation: gql`
        mutation downvote($id: String!) {
          downvote(id: $id) {
            id
            title
            voteCount
          }
        }
      `,
      variables: {
        id: id
      }
    });
  }
}
